import tag from './tag';
import { parseISO } from 'date-fns';
import { zonedTimeToUtc, utcToZonedTime, format } from 'date-fns-tz';

// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
const timeZone = 'Asia/Yakutsk'; // America/New_York
const pattern = 'dd.MM.yyyy HH:mm:ss.SSS \'GMT\' XXX (z)'

const testCases = {
    'a': '2021-07-09T06:00:00Z',
    'b': '2021-07-09T09:00:00+03:00',
    'c': '2021-07-09T05:00:00-01:00'
};

const trEl = tag('tr')
const tdEl = tag('td', 'border: 1px solid black;')
const tableEl = tag('table', 'width: 80%; border: 1px solid black;')
const rows = [];

for (const key in testCases) {
    const value = testCases[key];
    const parsedValue = parseISO(value);
    rows.push(trEl(
        tdEl(key),
        tdEl(value),
        tdEl(parsedValue),
        tdEl(parsedValue.toISOString()),
        tdEl(format(parsedValue, pattern, { timeZone })),
    ));
}

const rootEl = document.getElementById('root');
rootEl.appendChild(tableEl(...rows));
