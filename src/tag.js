export default (tagName, style = '') => {
    return (...children) => {
        const el = document.createElement(tagName);

        if (style) {
            el.setAttribute('style', style);
        }

        if (children) {
            children.forEach(item => {
                try {
                    el.appendChild(item);
                } catch (e) {
                    el.innerHTML += `${item}`;
                }
            });
        }

        return el;
    }
};